<?php

namespace Drupal\bitly_links\Service;

use Drupal\Core\Entity\EntityStorageException;

abstract class BitlyOperationTypes
{
  const ALL = 0;
  const UPDATE_MISSING = 1;
  const UPDATE_NODES = 2;
  const DELETE_NODES = 3;
  const DELETE_ALL = 4;

  public static function asString($operation): string
  {
    $operationString = array(
      self::ALL => "Add bitly link to all selected content type",
      self::UPDATE_MISSING => "Add missing bitly links for all selected content type",
      self::UPDATE_NODES => "Add bitly links for specified nodes",
      self::DELETE_NODES => "Delete bitly link for content type",
      self::DELETE_ALL => "Delete bitly link for all selected content type"
    );

    return $operationString[$operation];
  }
}

interface BulkOperationServiceInterface
{
    /**
     * @param $contentType
     *  The content type machine name
     * 
     * @param $operation
     *  The bulk operation being executed
     * 
     * @param $baseUrl
     * The baseURL (usually the site domain name)
     * 
     * @param array $nodes
     *  Array of node ids
     * 
     * @throws EntityStorageException
     *  This exception is thrown if there is a problem saving the updated node
     */
    public function generate($contentType, $operation, $baseUrl = NULL, array $nodes = array());
}
