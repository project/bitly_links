<?php

namespace Drupal\bitly_links\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Language\LanguageManagerInterface;

class BulkOperationManager implements BulkOperationServiceInterface
{
    /**
     * @var LanguageManagerInterface;
     */
    protected LanguageManagerInterface $languageManager;
    /**
     * @var AliasManager
     */
    protected AliasManager $aliasManager;
    /**
     * @var RequestContext
     */
    protected RequestContext $requestContext;
    /**
     * @var BitlyLinksServiceInterface;
     */
    protected BitlyLinksServiceInterface $bitlyLinksService;
    /**
     * @var EntityTypeManagerInterface
     */
    protected EntityTypeManagerInterface $entityTypeManager;
    public function __construct(BitlyLinksServiceInterface $bitlyLinksService, LanguageManagerInterface $languageManager,
                                AliasManager $aliasManager, RequestContext $requestContext, EntityTypeManagerInterface $entityTypeManager){
        $this->languageManager = $languageManager;
        $this->aliasManager = $aliasManager;
        $this->requestContext = $requestContext;
        $this->bitlyLinksService = $bitlyLinksService;
        $this->entityTypeManager = $entityTypeManager;
    }

    private function saveBitlyLink($node, $value)
    {
      $node->set('bitly_links_field', $value);
      $node->save();
    }

    private function getBitlyLink($node, $baseUrl)
    {
      $aliasPath = $this->getPathAlias($node);
      $response = $this->bitlyLinksService->shorten($baseUrl.$aliasPath);
      return json_decode($response)->link;
    }

    private function setLinkIfEmptyInNodeList(array $nodes, string $baseUrl): int
    {
      $count = 0;
      foreach ($nodes as $node) {
        $bitlyLink = self::getBitlyLink($node, $baseUrl);
        $hasBitlyLink = $node->get('bitly_links_field')->isEmpty();
        if ($hasBitlyLink) {
          self::saveBitlyLink($node, $bitlyLink);
          $count += 1;
        }
      }

      return $count;
    }

    private function setLinkEmptyInNodeList(array $nodes)
    {
      $count = 0;
      foreach ($nodes as $node) {
        self::saveBitlyLink($node, '');
        $count += 1;
      }

      return $count;
    }

    private function setLinkInNodeList(array $nodes, $baseUrl)
    {
      $count = 0;
      foreach ($nodes as $node) {
        $bitlyLink = self::getBitlyLink($node, $baseUrl);
        self::saveBitlyLink($node, $bitlyLink);
        $count += 1;
      }

      return $count;
    }

  private function getPathAlias($node){
        $systemPath = '/node/'.$node->id();
        $langCode = $this->languageManager->getCurrentLanguage()->getId();
        $alias = $this->aliasManager->getAliasByPath($systemPath, $langCode);
        if (!isset($alias)){
            return $systemPath;
        }
        return $alias;
    }

    /**
     * @inheritdoc
     */
    public function generate($contentType, $operation, $baseUrl = NULL, array $nodes = array()): string
    {
        $storage = $this->entityTypeManager->getStorage('node');
        if (!isset($baseUrl)){
          $find = array("localhost", "127.0.0.1",);
          $replace = array("drupal.test");
          $baseUrl = $this->requestContext->getCompleteBaseUrl();
          $baseUrl = str_replace($find, $replace, $baseUrl);
        }

        /* Check if we are doing specific nodes, or all nodes */
        if (empty($nodes)) {
          $query = $storage->getQuery()->condition('type', $contentType);
          $ids = $query->execute();
          $nodes = $storage->loadMultiple($ids);
        } else {
          $nodes = $storage->loadMultiple($nodes);
        }

        switch ($operation) {
          case BitlyOperationTypes::UPDATE_NODES:
          case BitlyOperationTypes::ALL:
            $nodesDone = self::setLinkInNodeList($nodes, $baseUrl);
            $status = 'Finished ' . BitlyOperationTypes::asString($operation) . ', on ' . $nodesDone . ' nodes';
            break;
          case BitlyOperationTypes::UPDATE_MISSING:
            $nodesDone = self::setLinkIfEmptyInNodeList($nodes, $baseUrl);
            $status = 'Finished ' . BitlyOperationTypes::asString($operation) . ', on ' . $nodesDone . ' nodes';
            break;
          case BitlyOperationTypes::DELETE_NODES:
          case BitlyOperationTypes::DELETE_ALL:
            $nodesDone = self::setLinkEmptyInNodeList($nodes);
            $status = 'Finished ' . BitlyOperationTypes::asString($operation) . ', on ' . $nodesDone . ' nodes';
            break;
          default:
            $status = 'Err: Operation type ' . $operation . ' failed';
            break;
        }

      return $status;
    }
}
